/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Modelo;

/**
 *
 * @author Patrick
 */
public class ObjetoVuelo {
    private String numeroVuelo;
    private String nombreAerolinea;
    private String tamañoAvion;
    private String estado;
    private String iataProcedencia;
    private Integer idArea;
    private String tipoArea;
    private String tamanoArea;
    private String tiempoProgramado;
    private String tiempoEstimado;
    private String tiempoLlegada;

    public ObjetoVuelo() {
    }

    public String getNumeroVuelo() {
        return numeroVuelo;
    }

    public void setNumeroVuelo(String numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public String getNombreAerolinea() {
        return nombreAerolinea;
    }

    public void setNombreAerolinea(String nombreAerolinea) {
        this.nombreAerolinea = nombreAerolinea;
    }

    public String getTamañoAvion() {
        return tamañoAvion;
    }

    public void setTamañoAvion(String tamañoAvion) {
        this.tamañoAvion = tamañoAvion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIataProcedencia() {
        return iataProcedencia;
    }

    public void setIataProcedencia(String iataProcedencia) {
        this.iataProcedencia = iataProcedencia;
    }

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }

    public String getTipoArea() {
        return tipoArea;
    }

    public void setTipoArea(String tipoArea) {
        this.tipoArea = tipoArea;
    }

    public String getTamanoArea() {
        return tamanoArea;
    }

    public void setTamanoArea(String tamanoArea) {
        this.tamanoArea = tamanoArea;
    }

    public String getTiempoProgramado() {
        return tiempoProgramado;
    }

    public void setTiempoProgramado(String tiempoProgramado) {
        this.tiempoProgramado = tiempoProgramado;
    }

    public String getTiempoEstimado() {
        return tiempoEstimado;
    }

    public void setTiempoEstimado(String tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }

    public String getTiempoLlegada() {
        return tiempoLlegada;
    }

    public void setTiempoLlegada(String tiempoLlegada) {
        this.tiempoLlegada = tiempoLlegada;
    }

}
