package com.grupoSCV;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@EnableScheduling
public class Application extends SpringBootServletInitializer{

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
//    @Bean
//    public WebMvcConfigurer corsConfigurer(){
//        return new WebMvcConfigurerAdapter(){
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**")
//                    .allowedOrigins("http://localhost:8080").allowedMethods("HEAD", "OPTIONS", "PUT", "POST", "GET", "PATH", "DELETE");
//    //                .allowedOrigins("*").allowedMethods("HEAD", "OPTIONS", "PUT", "POST", "GET", "PATH", "DELETE");
//            }
//        };
//    }
//    
//    @Bean
//    public RestTemplate customRestTemplate(){
//        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
//        httpRequestFactory.setConnectionRequestTimeout(5*60*1000);
//        httpRequestFactory.setConnectTimeout(5*60*1000);
//        httpRequestFactory.setReadTimeout(5*60*1000);
//        return new RestTemplate(httpRequestFactory);
//    }    
}