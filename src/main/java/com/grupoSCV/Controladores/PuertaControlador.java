/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Dao.PuertaDAO;
import com.grupoSCV.Entidades.Puerta;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/puerta")
public class PuertaControlador {
    
    @Autowired
    private PuertaDAO puertaDAO;
    
    @GetMapping("/obtenerTodos")
    public List<Puerta> obtenerTodasPuertas(){
        return puertaDAO.findAll();
    }
    
}
