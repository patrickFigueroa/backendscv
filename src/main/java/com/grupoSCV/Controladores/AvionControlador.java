/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Entidades.Aerolinea;
import com.grupoSCV.Entidades.Avion;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grupoSCV.Dao.AvionDAO;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/avion")
public class AvionControlador {
    
    @Autowired
    private AvionDAO avionDAO;
    
    @GetMapping("/obtenerTodos")
    public List<Avion> obtenerTodasAviones(){
        return avionDAO.findAll();
    }
    
    @GetMapping("/obtenerPor/{idAerolinea}")
    public ResponseEntity<Avion> obtenerPorIdAvion(@PathVariable(value="idAerolinea") Integer idAvion){
        Optional<Avion> avion = avionDAO.findById(idAvion);
        return ((avion.isPresent())? new ResponseEntity(avion.get(),HttpStatus.OK):new ResponseEntity(null,HttpStatus.NOT_FOUND));
    }
    
    @PostMapping("/crear")
    public ResponseEntity<Avion> crearAvion(@Valid @RequestBody Avion avion){
        return new ResponseEntity<>(avionDAO.save(avion),HttpStatus.OK);
    }
    
    @PutMapping("/actualizar")
    public ResponseEntity<Avion> actualizarAvion(@Valid @RequestBody Avion avion) {
        Avion avionGuardada = avionDAO.save(avion);
        return ((avionGuardada!=null)? new ResponseEntity(avionGuardada,HttpStatus.OK):new ResponseEntity(null,HttpStatus.CONFLICT));
    }
}
