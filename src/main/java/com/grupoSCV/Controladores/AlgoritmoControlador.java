///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package com.grupoSCV.Controladores;
// *
// * @author Patrick

import com.fasterxml.jackson.databind.ObjectMapper;
import com.grupoSCV.Componentes.ComponenteAlgo;
import com.grupoSCV.Modelo.ObjetoVuelo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/resultado")
public class AlgoritmoControlador {

    @Autowired
    private ComponenteAlgo compAlgo;

    @Scheduled(cron = "0 0/5 * * * *") //cada 5min
    public void pruebaAlgo() {
        String urlAlg = "http://192.168.214.177:5000/getjson";
//        String urlAlg = "http://localhost:5000/getjson";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(urlAlg, String.class);
//        System.out.println(result);
        compAlgo.setActual(result);
    }

    @GetMapping("/getResultado")
    public ResponseEntity<String> resultadoAlgo() {
        return new ResponseEntity<>(compAlgo.getActual(), HttpStatus.OK);
    }
}
