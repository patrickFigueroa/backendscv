/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Entidades.Usuario;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grupoSCV.Dao.UsuarioDAO;
import com.grupoSCV.Modelo.UsuarioLogin;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/usuario")
public class UsuarioControlador {
    
    @Autowired
    private UsuarioDAO usuarioDAO;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @GetMapping("/obtenerTodos")
    public List<Usuario> obtenerTodosUsuarios(){
        return usuarioDAO.findAll();
    }
    
    @GetMapping("/obtenerPor/{idUsuario}")
    public ResponseEntity<Usuario> obtenerPorIdUsuario(@PathVariable(value="idUsuario") Integer idUsuario){
        Optional<Usuario> usuario = usuarioDAO.findById(idUsuario);
        return ((usuario.isPresent())? new ResponseEntity(usuario.get(),HttpStatus.OK):new ResponseEntity(null,HttpStatus.NOT_FOUND));
    }
    
//    @PostMapping("/login")
//    public ResponseEntity<Usuario> login(@Valid @RequestBody Usuario usuario) {
//        Usuario usuarioHallado = usuarioDAO.findById(usuario.getIdUsuario()).get();
//        if(passwordEncoder.matches(usuario.getContrasena(), usuarioHallado.getContrasena())) 
//            return new ResponseEntity(usuarioHallado,HttpStatus.OK);
//        else return new ResponseEntity(null,HttpStatus.NOT_ACCEPTABLE);
//    }
    
    @PostMapping("/login")
    public ResponseEntity<Usuario> login(@Valid @RequestBody UsuarioLogin ul) {
        List<Usuario> listaUsuarios = usuarioDAO.findAll();
        for(Usuario user : listaUsuarios ){
            if(user.getUsuario().equals(ul.getUsuario()) && passwordEncoder.matches(ul.getContrasena(),user.getContrasena()))
                return new ResponseEntity(user,HttpStatus.OK);
        }
        return new ResponseEntity(null,HttpStatus.NOT_ACCEPTABLE);
    }
    
    @PostMapping("/crear")
    public ResponseEntity<Usuario> crearUsuario(@Valid @RequestBody Usuario usuario){
        String contraCod = passwordEncoder.encode(usuario.getContrasena());
        usuario.setContrasena(contraCod);
        return new ResponseEntity<>(usuarioDAO.save(usuario),HttpStatus.OK);
    }
    
    @PutMapping("/actualizar")
    public ResponseEntity<Usuario> actualizarUsuario(@RequestParam("flagContra") int flagContra,@Valid @RequestBody Usuario usuario) {
        //flagContra = 1 => actualizara el password entonces se debe codificar y setear el nuevo
        if(flagContra == 1){
            String contraCod = passwordEncoder.encode(usuario.getContrasena());
            usuario.setContrasena(contraCod);
        }            
        Usuario usuarioGuardada = usuarioDAO.save(usuario);
        return ((usuarioGuardada!=null)? new ResponseEntity(usuarioGuardada,HttpStatus.OK):new ResponseEntity(null,HttpStatus.CONFLICT));
    }
}