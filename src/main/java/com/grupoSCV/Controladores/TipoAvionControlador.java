/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Dao.TipoAvionDAO;
import com.grupoSCV.Entidades.TipoAvion;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/tipoAvion")
public class TipoAvionControlador {
    
    @Autowired
    private TipoAvionDAO tipoAvionDAO;
    
    @GetMapping("/obtenerTodos")
    public List<TipoAvion> obtenerTipoAvion(){
        return tipoAvionDAO.findAll();
    }
    
    @PostMapping("/crear")
    public ResponseEntity<TipoAvion> crearAvion(@Valid @RequestBody TipoAvion tipoAvion){
        return new ResponseEntity<>(tipoAvionDAO.save(tipoAvion),HttpStatus.OK);
    }
    
    @PutMapping("/actualizar")
    public ResponseEntity<TipoAvion> actualizarAvion(@Valid @RequestBody TipoAvion tipoAvion){
        TipoAvion tipoAvionGuardado = tipoAvionDAO.save(tipoAvion);
        return ((tipoAvionGuardado!=null)? new ResponseEntity(tipoAvionGuardado,HttpStatus.OK):new ResponseEntity(null,HttpStatus.CONFLICT));
    }
    
}
