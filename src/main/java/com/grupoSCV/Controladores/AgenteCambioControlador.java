/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Entidades.Aerolinea;
import com.grupoSCV.Entidades.AgenteCambio;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grupoSCV.Dao.AgenteCambioDAO;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/agenteCambio")
public class AgenteCambioControlador {
    
    @Autowired
    private AgenteCambioDAO agenteCambioDAO;
    
    @GetMapping("/obtenerTodos")
    public List<AgenteCambio> obtenerTodosAgenteCambios(){
        return agenteCambioDAO.findAll();
    }
    
    @GetMapping("/obtenerPor/{idAgenteCambio}")
    public ResponseEntity<AgenteCambio> obtenerPorIdAgenteCambio(@PathVariable(value="idAgenteCambio") Integer idAgenteCambio){
        Optional<AgenteCambio> data = agenteCambioDAO.findById(idAgenteCambio);
        return ((data.isPresent())? new ResponseEntity(data.get(),HttpStatus.OK):new ResponseEntity(null,HttpStatus.NOT_FOUND));
    }
    
    @PostMapping("/crear")
    public ResponseEntity<AgenteCambio> crearAgenteCambio(@Valid @RequestBody AgenteCambio agenteCambio){
        return new ResponseEntity<>(agenteCambioDAO.save(agenteCambio),HttpStatus.OK);
    }
    
    @PutMapping("/actualizar")
    public ResponseEntity<AgenteCambio> actualizarAgenteCambio(@Valid @RequestBody AgenteCambio agenteCambio) {
        AgenteCambio agenteCambioGuardado = agenteCambioDAO.save(agenteCambio);
        return ((agenteCambioGuardado!=null)? new ResponseEntity(agenteCambioGuardado,HttpStatus.OK):new ResponseEntity(null,HttpStatus.CONFLICT));
    }
}
