/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Controladores;

import com.grupoSCV.Dao.AerolineaDAO;
import com.grupoSCV.Entidades.Aerolinea;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping("/api/aerolinea")
public class AerolineaControlador {
 
    @Autowired
    private AerolineaDAO aerolineaDAO;
    
    @GetMapping("/obtenerTodos")
    public List<Aerolinea> obtenerTodasAerolineas(){
        return aerolineaDAO.findAll();
    }
    
    @GetMapping("/obtenerPor/{idAerolinea}")
    public ResponseEntity<Aerolinea> obtenerPorIdAerolinea(@PathVariable(value="idAerolinea") Integer idAerolinea){
        Optional<Aerolinea> aerolinea = aerolineaDAO.findById(idAerolinea);
        return ((aerolinea.isPresent())? new ResponseEntity(aerolinea.get(),HttpStatus.OK):new ResponseEntity(null,HttpStatus.NOT_FOUND));
    }
    
    @PostMapping("/crear")
    public ResponseEntity<Aerolinea> crearAerolinea(@Valid @RequestBody Aerolinea aerolinea){
        return new ResponseEntity<>(aerolineaDAO.save(aerolinea),HttpStatus.OK);
    }
    
    @PutMapping("/actualizar")
    public ResponseEntity<Aerolinea> actualizarAerolinea(@Valid @RequestBody Aerolinea aerolinea) {
        Aerolinea aerolineaGuardada = aerolineaDAO.save(aerolinea);
        return ((aerolineaGuardada!=null)? new ResponseEntity(aerolineaGuardada,HttpStatus.OK):new ResponseEntity(null,HttpStatus.CONFLICT));
    }
}
