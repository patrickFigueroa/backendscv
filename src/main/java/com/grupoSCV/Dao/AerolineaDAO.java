/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Dao;

import com.grupoSCV.Entidades.Aerolinea;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Patrick
 */
public interface AerolineaDAO extends JpaRepository<Aerolinea,Integer>{

    public Aerolinea findByNombre(String nombreAerolinea);
    
}
