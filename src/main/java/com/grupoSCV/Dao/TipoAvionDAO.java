/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Dao;

import com.grupoSCV.Entidades.TipoAvion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Patrick
 */
public interface TipoAvionDAO extends JpaRepository<TipoAvion,Integer>{

    public TipoAvion findByIata(String iata);
    
}
