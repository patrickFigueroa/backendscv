/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Componentes;

import org.springframework.stereotype.Component;

/**
 *
 * @author Patrick
 */
@Component
public class ComponenteAlgo {
    private String actual;

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    
}
