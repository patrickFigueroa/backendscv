/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tciudad_aeropuerto")
@NamedQueries({
    @NamedQuery(name = "CiudadAeropuerto.findAll", query = "SELECT c FROM CiudadAeropuerto c")})
public class CiudadAeropuerto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ciudad")
    private Integer idCiudad;
    
    @Size(max = 45)
    @Column(name = "iata")
    private String iata;
    
    @Size(max = 120)
    @Column(name = "nombre")
    private String nombre;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tciudadAeropuertoIdCiudad")
    private List<AeropuertoOrigen> aeropuertoOrigenList;

    public CiudadAeropuerto() {
    }

    public CiudadAeropuerto(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<AeropuertoOrigen> getAeropuertoOrigenList() {
        return aeropuertoOrigenList;
    }

    public void setAeropuertoOrigenList(List<AeropuertoOrigen> aeropuertoOrigenList) {
        this.aeropuertoOrigenList = aeropuertoOrigenList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCiudad != null ? idCiudad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CiudadAeropuerto)) {
            return false;
        }
        CiudadAeropuerto other = (CiudadAeropuerto) object;
        if ((this.idCiudad == null && other.idCiudad != null) || (this.idCiudad != null && !this.idCiudad.equals(other.idCiudad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.CiudadAeropuerto[ idCiudad=" + idCiudad + " ]";
    }
    
}
