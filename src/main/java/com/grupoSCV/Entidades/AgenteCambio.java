/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tagente_cambio")
@NamedQueries({
    @NamedQuery(name = "AgenteCambio.findAll", query = "SELECT a FROM AgenteCambio a")})
public class AgenteCambio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtagente_cambio")
    private Integer idtagenteCambio;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "nombre_tabla")
    private String nombreTabla;
    
    @Size(max = 200)
    @Column(name = "descripcion")
    private String descripcion;
    
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JoinColumn(name = "taerolinea_id_aerolinea", referencedColumnName = "id_aerolinea")
    @ManyToOne
    private Aerolinea taerolineaIdAerolinea;
    
    @JoinColumn(name = "tarea_id_area_estacionamiento", referencedColumnName = "id_area_estacionamiento")
    @ManyToOne
    private Area tareaIdAreaEstacionamiento;
    
    @JoinColumn(name = "tavion_id_Avion", referencedColumnName = "id_Avion")
    @ManyToOne
    private Avion tavionidAvion;
    
    @JoinColumn(name = "ttipo_avion_id_tipo_avion", referencedColumnName = "id_tipo_avion")
    @ManyToOne
    private TipoAvion ttipoAvionIdTipoAvion;
    
    @JoinColumn(name = "tusuario_id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuario tusuarioIdUsuario;

    public AgenteCambio() {
    }

    public AgenteCambio(Integer idtagenteCambio) {
        this.idtagenteCambio = idtagenteCambio;
    }

    public AgenteCambio(Integer idtagenteCambio, String nombreTabla) {
        this.idtagenteCambio = idtagenteCambio;
        this.nombreTabla = nombreTabla;
    }

    public Integer getIdtagenteCambio() {
        return idtagenteCambio;
    }

    public void setIdtagenteCambio(Integer idtagenteCambio) {
        this.idtagenteCambio = idtagenteCambio;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public Aerolinea getTaerolineaIdAerolinea() {
        return taerolineaIdAerolinea;
    }

    public void setTaerolineaIdAerolinea(Aerolinea taerolineaIdAerolinea) {
        this.taerolineaIdAerolinea = taerolineaIdAerolinea;
    }

    public Area getTareaIdAreaEstacionamiento() {
        return tareaIdAreaEstacionamiento;
    }

    public void setTareaIdAreaEstacionamiento(Area tareaIdAreaEstacionamiento) {
        this.tareaIdAreaEstacionamiento = tareaIdAreaEstacionamiento;
    }

    public Avion getTavionidAvion() {
        return tavionidAvion;
    }

    public void setTavionidAvion(Avion tavionidAvion) {
        this.tavionidAvion = tavionidAvion;
    }

    public TipoAvion getTtipoAvionIdTipoAvion() {
        return ttipoAvionIdTipoAvion;
    }

    public void setTtipoAvionIdTipoAvion(TipoAvion ttipoAvionIdTipoAvion) {
        this.ttipoAvionIdTipoAvion = ttipoAvionIdTipoAvion;
    }

    public Usuario getTusuarioIdUsuario() {
        return tusuarioIdUsuario;
    }

    public void setTusuarioIdUsuario(Usuario tusuarioIdUsuario) {
        this.tusuarioIdUsuario = tusuarioIdUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtagenteCambio != null ? idtagenteCambio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgenteCambio)) {
            return false;
        }
        AgenteCambio other = (AgenteCambio) object;
        if ((this.idtagenteCambio == null && other.idtagenteCambio != null) || (this.idtagenteCambio != null && !this.idtagenteCambio.equals(other.idtagenteCambio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.AgenteCambio[ idtagenteCambio=" + idtagenteCambio + " ]";
    }
    
}
