/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tavion")
@NamedQueries({
    @NamedQuery(name = "Avion.findAll", query = "SELECT a FROM Avion a")})
public class Avion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Avion")
    private Integer idAvion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "reg_nro")
    private String regNro;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "iata")
    private String iata;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "icao")
    private String icao;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tavionidAvion")
    private List<Vuelo> vueloList;
    
    @JsonIgnore
    @OneToMany(mappedBy = "tavionidAvion")
    private List<AgenteCambio> agenteCambioList;
    
    @JoinColumn(name = "taerolinea_id_aerolinea", referencedColumnName = "id_aerolinea")
    @ManyToOne(optional = false)
    private Aerolinea taerolineaIdAerolinea;
    
    @JoinColumn(name = "ttipo_avion_id_tipo_avion", referencedColumnName = "id_tipo_avion")
    @ManyToOne(optional = false)
    private TipoAvion ttipoAvionIdTipoAvion;

    public Avion() {
    }

    public Avion(Integer idAvion) {
        this.idAvion = idAvion;
    }

    public Avion(Integer idAvion, String regNro, String iata, String icao) {
        this.idAvion = idAvion;
        this.regNro = regNro;
        this.iata = iata;
        this.icao = icao;
    }

    public Integer getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(Integer idAvion) {
        this.idAvion = idAvion;
    }

    public String getRegNro() {
        return regNro;
    }

    public void setRegNro(String regNro) {
        this.regNro = regNro;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public List<Vuelo> getVueloList() {
        return vueloList;
    }

    public void setVueloList(List<Vuelo> vueloList) {
        this.vueloList = vueloList;
    }

    public List<AgenteCambio> getAgenteCambioList() {
        return agenteCambioList;
    }

    public void setAgenteCambioList(List<AgenteCambio> agenteCambioList) {
        this.agenteCambioList = agenteCambioList;
    }

    public Aerolinea getTaerolineaIdAerolinea() {
        return taerolineaIdAerolinea;
    }

    public void setTaerolineaIdAerolinea(Aerolinea taerolineaIdAerolinea) {
        this.taerolineaIdAerolinea = taerolineaIdAerolinea;
    }

    public TipoAvion getTtipoAvionIdTipoAvion() {
        return ttipoAvionIdTipoAvion;
    }

    public void setTtipoAvionIdTipoAvion(TipoAvion ttipoAvionIdTipoAvion) {
        this.ttipoAvionIdTipoAvion = ttipoAvionIdTipoAvion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAvion != null ? idAvion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Avion)) {
            return false;
        }
        Avion other = (Avion) object;
        if ((this.idAvion == null && other.idAvion != null) || (this.idAvion != null && !this.idAvion.equals(other.idAvion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.Avion[ idAvion=" + idAvion + " ]";
    }
    
}
