/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "taerolinea")
@NamedQueries({
    @NamedQuery(name = "Aerolinea.findAll", query = "SELECT a FROM Aerolinea a")})
public class Aerolinea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_aerolinea")
    private Integer idAerolinea;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "nombre")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "iata")
    private String iata;
    
    @Size(max = 45)
    @Column(name = "icao")
    private String icao;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JsonIgnore
    @OneToMany(mappedBy = "taerolineaIdAerolinea")
    private List<AgenteCambio> agenteCambioList;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taerolineaIdAerolinea")
    private List<Avion> avionList;

    public Aerolinea() {
    }

    public Aerolinea(Integer idAerolinea) {
        this.idAerolinea = idAerolinea;
    }

    public Aerolinea(Integer idAerolinea, String nombre, String iata) {
        this.idAerolinea = idAerolinea;
        this.nombre = nombre;
        this.iata = iata;
    }

    public Integer getIdAerolinea() {
        return idAerolinea;
    }

    public void setIdAerolinea(Integer idAerolinea) {
        this.idAerolinea = idAerolinea;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public List<AgenteCambio> getAgenteCambioList() {
        return agenteCambioList;
    }

    public void setAgenteCambioList(List<AgenteCambio> agenteCambioList) {
        this.agenteCambioList = agenteCambioList;
    }

    public List<Avion> getAvionList() {
        return avionList;
    }

    public void setAvionList(List<Avion> avionList) {
        this.avionList = avionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAerolinea != null ? idAerolinea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aerolinea)) {
            return false;
        }
        Aerolinea other = (Aerolinea) object;
        if ((this.idAerolinea == null && other.idAerolinea != null) || (this.idAerolinea != null && !this.idAerolinea.equals(other.idAerolinea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.Aerolinea[ idAerolinea=" + idAerolinea + " ]";
    }
    
}
