/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tpuerta")
@NamedQueries({
    @NamedQuery(name = "Puerta.findAll", query = "SELECT p FROM Puerta p")})
public class Puerta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_puerta")
    private Integer idPuerta;
    
    @Size(max = 80)
    @Column(name = "descripcion")
    private String descripcion;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "coordenada_x")
    private Double coordenadaX;
    
    @Column(name = "coordenada_y")
    private Double coordenadaY;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tpuertaIdPuerta")
    private List<Area> areaList;

    public Puerta() {
    }

    public Puerta(Integer idPuerta) {
        this.idPuerta = idPuerta;
    }
    
//    public void addArea(Area area){
//        areaList.add(area);
//    }
    
    public Integer getIdPuerta() {
        return idPuerta;
    }

    public void setIdPuerta(Integer idPuerta) {
        this.idPuerta = idPuerta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(Double coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    public Double getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(Double coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPuerta != null ? idPuerta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Puerta)) {
            return false;
        }
        Puerta other = (Puerta) object;
        if ((this.idPuerta == null && other.idPuerta != null) || (this.idPuerta != null && !this.idPuerta.equals(other.idPuerta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.Puerta[ idPuerta=" + idPuerta + " ]";
    }
    
}
