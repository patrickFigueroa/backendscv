/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "taeropuerto_origen")
@NamedQueries({
    @NamedQuery(name = "AeropuertoOrigen.findAll", query = "SELECT a FROM AeropuertoOrigen a")})
public class AeropuertoOrigen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_aeropuerto")
    private Integer idAeropuerto;
    
    @Size(max = 120)
    @Column(name = "nombre")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "pais")
    private String pais;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taeropuertoIdAeropuerto")
    private List<Vuelo> vueloList;
    
    @JoinColumn(name = "tciudad_aeropuerto_id_ciudad", referencedColumnName = "id_ciudad")
    @ManyToOne(optional = false)
    private CiudadAeropuerto tciudadAeropuertoIdCiudad;

    public AeropuertoOrigen() {
    }

    public AeropuertoOrigen(Integer idAeropuerto) {
        this.idAeropuerto = idAeropuerto;
    }

    public AeropuertoOrigen(Integer idAeropuerto, String pais) {
        this.idAeropuerto = idAeropuerto;
        this.pais = pais;
    }

    public Integer getIdAeropuerto() {
        return idAeropuerto;
    }

    public void setIdAeropuerto(Integer idAeropuerto) {
        this.idAeropuerto = idAeropuerto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public List<Vuelo> getVueloList() {
        return vueloList;
    }

    public void setVueloList(List<Vuelo> vueloList) {
        this.vueloList = vueloList;
    }

    public CiudadAeropuerto getTciudadAeropuertoIdCiudad() {
        return tciudadAeropuertoIdCiudad;
    }

    public void setTciudadAeropuertoIdCiudad(CiudadAeropuerto tciudadAeropuertoIdCiudad) {
        this.tciudadAeropuertoIdCiudad = tciudadAeropuertoIdCiudad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAeropuerto != null ? idAeropuerto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AeropuertoOrigen)) {
            return false;
        }
        AeropuertoOrigen other = (AeropuertoOrigen) object;
        if ((this.idAeropuerto == null && other.idAeropuerto != null) || (this.idAeropuerto != null && !this.idAeropuerto.equals(other.idAeropuerto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.AeropuertoOrigen[ idAeropuerto=" + idAeropuerto + " ]";
    }
    
}
