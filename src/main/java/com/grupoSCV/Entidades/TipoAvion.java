/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "ttipo_avion")
@NamedQueries({
    @NamedQuery(name = "TipoAvion.findAll", query = "SELECT t FROM TipoAvion t")})
public class TipoAvion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_avion")
    private Integer idTipoAvion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "modelo")
    private String modelo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "iata")
    private String iata;
    
    @Size(max = 20)
    @Column(name = "tamano")
    private String tamano;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JsonIgnore
    @OneToMany(mappedBy = "ttipoAvionIdTipoAvion")
    private List<AgenteCambio> agenteCambioList;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ttipoAvionIdTipoAvion")
    private List<Avion> avionList;

    public TipoAvion() {
    }

    public TipoAvion(Integer idTipoAvion) {
        this.idTipoAvion = idTipoAvion;
    }

    public TipoAvion(Integer idTipoAvion, String modelo, String iata) {
        this.idTipoAvion = idTipoAvion;
        this.modelo = modelo;
        this.iata = iata;
    }

    public Integer getIdTipoAvion() {
        return idTipoAvion;
    }

    public void setIdTipoAvion(Integer idTipoAvion) {
        this.idTipoAvion = idTipoAvion;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public List<AgenteCambio> getAgenteCambioList() {
        return agenteCambioList;
    }

    public void setAgenteCambioList(List<AgenteCambio> agenteCambioList) {
        this.agenteCambioList = agenteCambioList;
    }

    public List<Avion> getAvionList() {
        return avionList;
    }

    public void setAvionList(List<Avion> avionList) {
        this.avionList = avionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoAvion != null ? idTipoAvion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoAvion)) {
            return false;
        }
        TipoAvion other = (TipoAvion) object;
        if ((this.idTipoAvion == null && other.idTipoAvion != null) || (this.idTipoAvion != null && !this.idTipoAvion.equals(other.idTipoAvion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.TipoAvion[ idTipoAvion=" + idTipoAvion + " ]";
    }
    
}
