/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tarea")
@NamedQueries({
    @NamedQuery(name = "Area.findAll", query = "SELECT a FROM Area a")})
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area_estacionamiento")
    private Integer idAreaEstacionamiento;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "coordenada_x")
    private double coordenadaX;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "coordenada_y")
    private double coordenadaY;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "tamano")
    private Double tamano;
    
    @Size(max = 25)
    @Column(name = "tipo_area")
    private String tipoArea;
    
    @Column(name = "ocupado")
    private Boolean ocupado;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JoinColumn(name = "tpuerta_id_puerta", referencedColumnName = "id_puerta")
    @ManyToOne(optional = false)
    private Puerta tpuertaIdPuerta;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tareaIdAreaEstacionamiento")
    private List<Vuelo> vueloList;
    
    @JsonIgnore
    @OneToMany(mappedBy = "tareaIdAreaEstacionamiento")
    private List<AgenteCambio> agenteCambioList;

    public Area() {
    }

    public Area(Integer idAreaEstacionamiento) {
        this.idAreaEstacionamiento = idAreaEstacionamiento;
    }

    public Area(Integer idAreaEstacionamiento, double coordenadaX, double coordenadaY) {
        this.idAreaEstacionamiento = idAreaEstacionamiento;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
    }

    public Integer getIdAreaEstacionamiento() {
        return idAreaEstacionamiento;
    }

    public void setIdAreaEstacionamiento(Integer idAreaEstacionamiento) {
        this.idAreaEstacionamiento = idAreaEstacionamiento;
    }

    public double getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(double coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    public double getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(double coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public Double getTamano() {
        return tamano;
    }

    public void setTamano(Double tamano) {
        this.tamano = tamano;
    }

    public String getTipoArea() {
        return tipoArea;
    }

    public void setTipoArea(String tipoArea) {
        this.tipoArea = tipoArea;
    }

    public Boolean getOcupado() {
        return ocupado;
    }

    public void setOcupado(Boolean ocupado) {
        this.ocupado = ocupado;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public Puerta getTpuertaIdPuerta() {
        return tpuertaIdPuerta;
    }

    public void setTpuertaIdPuerta(Puerta tpuertaIdPuerta) {
        this.tpuertaIdPuerta = tpuertaIdPuerta;
    }

    public List<Vuelo> getVueloList() {
        return vueloList;
    }

    public void setVueloList(List<Vuelo> vueloList) {
        this.vueloList = vueloList;
    }

    public List<AgenteCambio> getAgenteCambioList() {
        return agenteCambioList;
    }

    public void setAgenteCambioList(List<AgenteCambio> agenteCambioList) {
        this.agenteCambioList = agenteCambioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAreaEstacionamiento != null ? idAreaEstacionamiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.idAreaEstacionamiento == null && other.idAreaEstacionamiento != null) || (this.idAreaEstacionamiento != null && !this.idAreaEstacionamiento.equals(other.idAreaEstacionamiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.Area[ idAreaEstacionamiento=" + idAreaEstacionamiento + " ]";
    }
    
}
