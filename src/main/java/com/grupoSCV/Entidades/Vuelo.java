/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupoSCV.Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "tvuelo")
@NamedQueries({
    @NamedQuery(name = "Vuelo.findAll", query = "SELECT v FROM Vuelo v")})
public class Vuelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_vuelo")
    private Integer idVuelo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "iata")
    private String iata;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "icao")
    private String icao;
    
    @Size(max = 45)
    @Column(name = "numero_vuelo")
    private String numeroVuelo;
    
    @Column(name = "fecha_hora_real")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraReal;
    
    @Column(name = "fecha_hora_programada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraProgramada;
    
    @Size(max = 50)
    @Column(name = "estado_vuelo")
    private String estadoVuelo;
    
    @Column(name = "es_eliminado")
    private Boolean esEliminado;
    
    @JoinColumn(name = "taeropuerto_id_aeropuerto", referencedColumnName = "id_aeropuerto")
    @ManyToOne(optional = false)
    private AeropuertoOrigen taeropuertoIdAeropuerto;
    
    @JoinColumn(name = "tarea_id_area_estacionamiento", referencedColumnName = "id_area_estacionamiento")
    @ManyToOne(optional = false)
    private Area tareaIdAreaEstacionamiento;
    
    @JoinColumn(name = "tavion_idAvion", referencedColumnName = "id_Avion")
    @ManyToOne(optional = false)
    private Avion tavionidAvion;

    public Vuelo() {
    }

    public Vuelo(Integer idVuelo) {
        this.idVuelo = idVuelo;
    }

    public Vuelo(Integer idVuelo, String iata, String icao) {
        this.idVuelo = idVuelo;
        this.iata = iata;
        this.icao = icao;
    }

    public Integer getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(Integer idVuelo) {
        this.idVuelo = idVuelo;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getNumeroVuelo() {
        return numeroVuelo;
    }

    public void setNumeroVuelo(String numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public Date getFechaHoraReal() {
        return fechaHoraReal;
    }

    public void setFechaHoraReal(Date fechaHoraReal) {
        this.fechaHoraReal = fechaHoraReal;
    }

    public Date getFechaHoraProgramada() {
        return fechaHoraProgramada;
    }

    public void setFechaHoraProgramada(Date fechaHoraProgramada) {
        this.fechaHoraProgramada = fechaHoraProgramada;
    }

    public String getEstadoVuelo() {
        return estadoVuelo;
    }

    public void setEstadoVuelo(String estadoVuelo) {
        this.estadoVuelo = estadoVuelo;
    }

    public Boolean getEsEliminado() {
        return esEliminado;
    }

    public void setEsEliminado(Boolean esEliminado) {
        this.esEliminado = esEliminado;
    }

    public AeropuertoOrigen getTaeropuertoIdAeropuerto() {
        return taeropuertoIdAeropuerto;
    }

    public void setTaeropuertoIdAeropuerto(AeropuertoOrigen taeropuertoIdAeropuerto) {
        this.taeropuertoIdAeropuerto = taeropuertoIdAeropuerto;
    }

    public Area getTareaIdAreaEstacionamiento() {
        return tareaIdAreaEstacionamiento;
    }

    public void setTareaIdAreaEstacionamiento(Area tareaIdAreaEstacionamiento) {
        this.tareaIdAreaEstacionamiento = tareaIdAreaEstacionamiento;
    }

    public Avion getTavionidAvion() {
        return tavionidAvion;
    }

    public void setTavionidAvion(Avion tavionidAvion) {
        this.tavionidAvion = tavionidAvion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVuelo != null ? idVuelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vuelo)) {
            return false;
        }
        Vuelo other = (Vuelo) object;
        if ((this.idVuelo == null && other.idVuelo != null) || (this.idVuelo != null && !this.idVuelo.equals(other.idVuelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.grupoSCV.Entidades.Vuelo[ idVuelo=" + idVuelo + " ]";
    }
    
}
